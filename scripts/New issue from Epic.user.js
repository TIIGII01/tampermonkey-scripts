// ==UserScript==
// @name         New issue from Epic
// @namespace    https://gitlab.com/
// @version      0.1
// @description  Open a new issue from an Epic
// @author       Viktor Nagy
// @homepage     https://nagyv-gitlab.gitlab.io/tampermonkey-scripts/
// @supportURL   https://gitlab.com/nagyv-gitlab/tampermonkey-scripts
// @match        https://gitlab.com/*/-/epics/*
// @grant        none
// @require      https://code.jquery.com/jquery-3.4.1.slim.min.js
// ==/UserScript==

(function() {
    'use strict';

    const defaultFallbackHandler = {
        get: function(target, name) {
            return target.hasOwnProperty(name) ? target[name] : target['gitlab-org'];
        }
    };
    const projectIssueUrls = new Proxy({
        'configure': 'https://gitlab.com/gitlab-org/configure/general/issues/new',
        'gitlab-org': 'https://gitlab.com/gitlab-org/gitlab/issues/new?issuable_template=Feature_proposal',
        'gitlab-com': 'https://gitlab.com/gitlab-com/www-gitlab-com/issues/new'
    }, defaultFallbackHandler);

    // Your code here...
    /*
    Given I'm on an epic page
    When I click the New issue button
    Then a new issue page opens
    and there is a quick-link to add the issue to the previous epic
    */

    function getNewIssueUrl(project){
         return projectIssueUrls[project];
    }

    function handleClick(ev) {
        const epicId = window.location.href.split('/').slice(-1);
        const epicTitle = $('.title.qa-title').first().text();
        const project = window.location.href.split('/').slice(-4, -3);

        window.localStorage.setItem('last-epic', JSON.stringify([epicId, epicTitle]));
        window.open(getNewIssueUrl(project), '_blank');
    }

    // Add button beside the Close epic button
    $('.detail-page-header-actions').append('<button type="button" id="vnagy-issue-from-epic" class="btn btn-grouped js-btn-epic-action"><!----> <span class="js-loading-button-label"> New issue </span></button>');
    $('#vnagy-issue-from-epic').click(handleClick);

    // Handle button click
    // save epic number and title as a cookie
    // open the new issue page
})();
