// ==UserScript==
// @name         Add issues button should open a new issue instead of some weird issue list
// @namespace    https://gitlab.com/
// @version      0.2
// @description  On board views the "Add issues" button should open a new issue instead of some weird issue list
// @author       Viktor Nagy
// @homepage     https://nagyv-gitlab.gitlab.io/tampermonkey-scripts/
// @supportURL   https://gitlab.com/nagyv-gitlab/tampermonkey-scripts
// @match        https://gitlab.com/*/-/boards*
// @grant        none
// @require      https://code.jquery.com/jquery-3.4.1.slim.min.js
// ==/UserScript==

(function() {
    'use strict';

    const project = window.location.pathname.split('/-/')[0]
    let path
    if(project.indexOf('/groups') === 0) {
        path = `${project}/-/issues`
    } else {
        path = `${project}/issues/new`
    }
    $('.board-extra-actions').first().html(`<a type="button" data-placement="bottom" title="Create new issue" href="${path}" class="btn btn-success prepend-left-10">New issue</a>`)
    window.setTimeout(() => {

    }, 500)
    
})();
